// Import necessary modules
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define a struct to represent the input data
#[derive(Deserialize, Serialize)]
struct Input {
    uppercase: String, // Field to hold the input data
}

// Define a struct to represent the output data
#[derive(Deserialize, Serialize)]
struct Output {
    modified_data: String, // Field to hold the processed result (data with periods replaced by exclamation marks)
}

// Main function, entry point of the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data); // Create a handler function from the process_data function
    lambda_runtime::run(func).await?; // Run the Lambda function
    Ok(()) // Return Ok if everything executed successfully
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let modified_data = event.uppercase.replace(".", "!"); // Replace all periods with exclamation marks
    Ok(Output { modified_data }) // Return the processed result
}
