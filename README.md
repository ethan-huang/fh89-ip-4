# Rust AWS Lambda and Step Functions for Simple Text Analysis

This project aims to develope a text data processing pipeline using two connected lambda function: the `uppercase` function and `exmark` function. The first function is to turn all the alphabets in a sentence into uppercase, and the second function is to make all of the period in a sentence to the exclamation mark.

## Goals

- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Connected data processing pipeline

## Steps

### Step 1: Create Lambda Functions

1. Initializing new AWS Lambda project in Rust using command line `cargo lambda new <PROJECT_NAME>` in terminal.

```
cargo lambda new uppercase
```

```
cargo lambda new exmark
```

2. Add necessary dependencies to `Cargo.toml` file.
3. Add functional implementations and inference endpoint in `main.rs` file.
4. Create the .json files and put at their corresponding project directory for local testing:

- data1.json

```
{"data": "hello, world. Patience is the key in life...."}
```

- data2.json

```
{"uppercase":"hello, world. Patience is the key in life..."}
```

5. Test these two lambda functions locally by running:

```
cd longest_word_identification
cargo lambda watch
cargo lambda invoke --data-file ./data/data1.json
```

```
cd counting_occurrences
cargo lambda watch
cargo lambda invoke --data-file ./data/data1.json
```

### Step 2: Deploy Lambda Functions to AWS

1. After succesfully testing the lambda functions, push these functions to the Gitlab.
2. Set the AWS access variable in `Settings` -> `CI/CD`.
3. Add the `.gitlab-ci.yml` file. Then, it will automatically deploy the lambda functions to AWS.

### Step 3: Orchestrate Step Functions Pipeline

1. Open the AWS Management Console and navigate to the AWS Step Functions page.
2. Create a new state machine in AWS Step Functions. Choose `AWS Lambda Invoke`.
3. Define the state machine as follows:

```
{
  "Comment": "A workflow to first to all of the alphabets to uppercase, and turn all of the periods to the exclamation mark.",
  "StartAt": "uppercase",
  "States": {
    "uppercase": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:851725513351:function:uppercase",
      "Next": "exmark"
    },
    "exmark": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:851725513351:function:exmark",
      "End": true
    }
  }
}

```

## Result

### Lambda Functions

- uppercase_function
  ![lambda_1](./image/f1.png)
- exclamation_mark_function
  ![lambda_2](./image/f2.png)

### State Machine

![state_machine_1](./image/f3.png)

- Execution Graph
  ![state_machine_2](./image/f4.png)

## Demo

- [Demo Video](https://gitlab.com/ethan-huang/fh89-ip-4/-/blob/main/ip4demo.mp4?ref_type=heads)
