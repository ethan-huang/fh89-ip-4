// Import necessary modules
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define a struct to represent the input data
#[derive(Deserialize, Serialize)]
struct Input {
    data: String, // Field to hold the input data
}

// Define a struct to represent the output data
#[derive(Deserialize, Serialize)]
struct Output {
    uppercase: String, // Field to hold the processed result (uppercase text)
}

// Main function, entry point of the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data); // Create a handler function from the process_data function
    lambda_runtime::run(func).await?; // Run the Lambda function
    Ok(()) // Return Ok if everything executed successfully
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let uppercase = event.data.to_uppercase(); // Convert input text to uppercase
    Ok(Output { uppercase }) // Return the processed result
}
